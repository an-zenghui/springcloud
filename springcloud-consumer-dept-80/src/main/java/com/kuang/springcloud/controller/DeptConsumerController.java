package com.kuang.springcloud.controller;

import com.kuang.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

/**
 * @Author 安增辉
 * @Date 2022/4/6 15:32
 * @Version 1.0
 */
@RestController
public class DeptConsumerController {
    //消费者 不应该有service
    //RestTemplate 供我们直接调用就可以 注册到spring中
    //（url，实体：map，class<T> responseType）
@Autowired
private RestTemplate restTemplate;

//private static final String REST_URL_PERFIX = "http://localhost:8001";
private static final String REST_URL_PERFIX = "http://SPRINGCLOUD-PROVIDER-DEPT";

@RequestMapping("/consumer/dept/get/{id}")
public Dept getDept(@PathVariable("id") Long id){
    return restTemplate.getForObject(REST_URL_PERFIX+"/dept/get/"+id,Dept.class);
}
@RequestMapping("/consumer/dept/add")
public Boolean addDept(Dept dept){
return restTemplate.postForObject(REST_URL_PERFIX+"/dept/add",dept,Boolean.class);
}
@RequestMapping("/consumer/get/list")
public List<Dept> queryAll(){
    return restTemplate.getForObject(REST_URL_PERFIX+"/dept/list",List.class);
}
}
