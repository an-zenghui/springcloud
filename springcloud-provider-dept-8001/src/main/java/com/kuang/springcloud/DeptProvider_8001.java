package com.kuang.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author 安增辉
 * @Date 2022/4/6 14:23
 * @Version 1.0
 */
//启动类
@SpringBootApplication
@EnableEurekaClient//在服务启动后自动注册到eureka中
public class DeptProvider_8001 {
    public static void main(String[] args) {
        SpringApplication.run(DeptProvider_8001.class,args);
    }
}
