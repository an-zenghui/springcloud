package com.kuang.springcloud.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author 安增辉
 * @Date 2022/4/6 13:11
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@Accessors(chain = true) //链式写法
public class Dept implements Serializable {
    /**
     * 主键部门号
     */
    private Long deptno;
    private String dname;
    //这个字段是存在哪个数据库的字段 微服务，，一个服务对应一个数据库，
    private String db_source;

    public Dept(String dname) {
        this.dname = dname;
    }
}
